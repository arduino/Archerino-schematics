Archerino - Schematics

This project is part of Archerino project developed at GOLEM
(http://golem.linux.it).
The project has been developed from Henk Jegers' ArcheryClock.

# Bill of materials

| Reference              |   Value                     |   Mount                 | Manufacturer no.
|------------------------|-----------------------------|-------------------------|-------------------------
| BT1                    |   Screw_Terminal_01x02      |   y                     |  TC0203620000G
| C3                     |   100u                      |   y                     |  647-UMW1E101MDD
| C5                     |   10u                       |   y                     |  647-UMT1C100MDD
| C1, C2, C4, C6, C7     |   100n                      |   y                     |  K104K15X7RF53H5
| J1-J5                  |   8P8C_Shielded             |   y                     |  54601-908WPLF
| J6, J7                 |   Screw_Terminal_01x04      |   y                     |  4x TC0203620000G
| Q1-Q4                  |   Q_NMOS_GDS                |   y                     |  IRFZ24NPBF
| R1, R3-R5              |   100                       |   y                     |  CFR-25JT-52-100R
| R2, R6, R7, R11        |   100k                      |   y                     |  CFR-25JR-52-100K
| R8, R9, R13            |   10k                       |   y                     |  CFR-25JR-52-10K
| R10                    |   2k7                       |   opt (Xbee)            |
| R12                    |   3k3                       |   opt (Xbee)            |
| U1-U5                  |   ULN2803A                  |   y                     |  ULN2803A
| U6                     |   L7808                     |   y                     |  L7808CV


Plus, you will need some:
- male 2.54mm pin heades, better if 90° angled, for SW1-SW3;
- male 2.54mm pin headers for arduino shield;
- female 02x04 pin socket for U8 (if you want to use nRF module instead of Xbee).

# License

Schematics and documentations are covered under
CERN OHL v.1.2 License.
