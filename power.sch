EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title "Archerino"
Date "2021-02-16"
Rev "rev2.0"
Comp "GOLEM"
Comment1 "by giuliof"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_Array:ULN2802A U?
U 1 1 5C742738
P 2600 1100
AR Path="/5C742738" Ref="U?"  Part="1" 
AR Path="/5C73A3FE/5C742738" Ref="U1"  Part="1" 
F 0 "U1" H 2300 1550 50  0000 L CNN
F 1 "ULN2803A" H 2900 1550 50  0000 R CNN
F 2 "Package_DIP:DIP-18_W7.62mm_LongPads" H 2650 450 50  0001 L CNN
F 3 "http://www.promelec.ru/pdf/1536.pdf" H 2700 900 50  0001 C CNN
F 5 "ULN2803A" H 0   0   50  0001 C CNN "Manufacturer ref."
F 6 "y" H 2600 1100 50  0001 C CNN "Mount"
	1    2600 1100
	1    0    0    -1  
$EndComp
$Comp
L Transistor_Array:ULN2802A U?
U 1 1 5C74273F
P 2600 2550
AR Path="/5C74273F" Ref="U?"  Part="1" 
AR Path="/5C73A3FE/5C74273F" Ref="U2"  Part="1" 
F 0 "U2" H 2300 3000 50  0000 L CNN
F 1 "ULN2803A" H 2900 3000 50  0000 R CNN
F 2 "Package_DIP:DIP-18_W7.62mm_LongPads" H 2650 1900 50  0001 L CNN
F 3 "http://www.promelec.ru/pdf/1536.pdf" H 2700 2350 50  0001 C CNN
F 4 "ULN2803A" H 2600 2550 50  0001 C CNN "Manufacturer ref."
F 6 "y" H 2600 2550 50  0001 C CNN "Mount"
	1    2600 2550
	1    0    0    -1  
$EndComp
$Comp
L Transistor_Array:ULN2802A U?
U 1 1 5C742746
P 2600 4000
AR Path="/5C742746" Ref="U?"  Part="1" 
AR Path="/5C73A3FE/5C742746" Ref="U3"  Part="1" 
F 0 "U3" H 2300 4450 50  0000 L CNN
F 1 "ULN2803A" H 2900 4450 50  0000 R CNN
F 2 "Package_DIP:DIP-18_W7.62mm_LongPads" H 2650 3350 50  0001 L CNN
F 3 "http://www.promelec.ru/pdf/1536.pdf" H 2700 3800 50  0001 C CNN
F 4 "ULN2803A" H 2600 4000 50  0001 C CNN "Manufacturer ref."
F 6 "y" H 2600 4000 50  0001 C CNN "Mount"
	1    2600 4000
	1    0    0    -1  
$EndComp
$Comp
L Transistor_Array:ULN2802A U?
U 1 1 5C74274D
P 2600 5450
AR Path="/5C74274D" Ref="U?"  Part="1" 
AR Path="/5C73A3FE/5C74274D" Ref="U4"  Part="1" 
F 0 "U4" H 2300 5900 50  0000 L CNN
F 1 "ULN2803A" H 2900 5900 50  0000 R CNN
F 2 "Package_DIP:DIP-18_W7.62mm_LongPads" H 2650 4800 50  0001 L CNN
F 3 "http://www.promelec.ru/pdf/1536.pdf" H 2700 5250 50  0001 C CNN
F 4 "ULN2803A" H 2600 5450 50  0001 C CNN "Manufacturer ref."
F 6 "y" H 2600 5450 50  0001 C CNN "Mount"
	1    2600 5450
	1    0    0    -1  
$EndComp
$Comp
L Transistor_Array:ULN2802A U?
U 1 1 5C742754
P 2600 6900
AR Path="/5C742754" Ref="U?"  Part="1" 
AR Path="/5C73A3FE/5C742754" Ref="U5"  Part="1" 
F 0 "U5" H 2300 7350 50  0000 L CNN
F 1 "ULN2803A" H 2900 7350 50  0000 R CNN
F 2 "Package_DIP:DIP-18_W7.62mm_LongPads" H 2650 6250 50  0001 L CNN
F 3 "http://www.promelec.ru/pdf/1536.pdf" H 2700 6700 50  0001 C CNN
F 4 "ULN2803A" H 2600 6900 50  0001 C CNN "Manufacturer ref."
F 6 "y" H 2600 6900 50  0001 C CNN "Mount"
	1    2600 6900
	1    0    0    -1  
$EndComp
$Comp
L Connector:8P8C J?
U 1 1 5C74275B
P 4200 1300
AR Path="/5C74275B" Ref="J?"  Part="1" 
AR Path="/5C73A3FE/5C74275B" Ref="J1"  Part="1" 
F 0 "J1" H 3871 1341 50  0000 R CNN
F 1 "8P8C_Shielded" H 3871 1250 50  0000 R CNN
F 2 "Connector_RJ:RJ45_Amphenol_54602-x08_Horizontal" V 4200 1325 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/144c/0900766b8144c29d.pdf" V 4200 1325 50  0001 C CNN
F 4 "54601-908WPLF" H 4200 1300 50  0001 C CNN "Manufacturer ref."
F 5 "y" H 4200 1300 50  0001 C CNN "Mount"
	1    4200 1300
	-1   0    0    -1  
$EndComp
$Comp
L Connector:8P8C J?
U 1 1 5C742762
P 4200 2750
AR Path="/5C742762" Ref="J?"  Part="1" 
AR Path="/5C73A3FE/5C742762" Ref="J2"  Part="1" 
F 0 "J2" H 3871 2791 50  0000 R CNN
F 1 "8P8C_Shielded" H 3871 2700 50  0000 R CNN
F 2 "Connector_RJ:RJ45_Amphenol_54602-x08_Horizontal" V 4200 2775 50  0001 C CNN
F 3 "~" V 4200 2775 50  0001 C CNN
F 4 "y" H 4200 2750 50  0001 C CNN "Mount"
F 5 "54601-908WPLF" H 4200 2750 50  0001 C CNN "Manufacturer ref."
	1    4200 2750
	-1   0    0    -1  
$EndComp
$Comp
L Connector:8P8C J?
U 1 1 5C742769
P 4200 4200
AR Path="/5C742769" Ref="J?"  Part="1" 
AR Path="/5C73A3FE/5C742769" Ref="J3"  Part="1" 
F 0 "J3" H 3871 4241 50  0000 R CNN
F 1 "8P8C_Shielded" H 3871 4150 50  0000 R CNN
F 2 "Connector_RJ:RJ45_Amphenol_54602-x08_Horizontal" V 4200 4225 50  0001 C CNN
F 3 "~" V 4200 4225 50  0001 C CNN
F 4 "y" H 4200 4200 50  0001 C CNN "Mount"
F 5 "54601-908WPLF" H 4200 4200 50  0001 C CNN "Manufacturer ref."
	1    4200 4200
	-1   0    0    -1  
$EndComp
$Comp
L Connector:8P8C J?
U 1 1 5C742770
P 4200 5650
AR Path="/5C742770" Ref="J?"  Part="1" 
AR Path="/5C73A3FE/5C742770" Ref="J4"  Part="1" 
F 0 "J4" H 3871 5691 50  0000 R CNN
F 1 "8P8C_Shielded" H 3871 5600 50  0000 R CNN
F 2 "Connector_RJ:RJ45_Amphenol_54602-x08_Horizontal" V 4200 5675 50  0001 C CNN
F 3 "~" V 4200 5675 50  0001 C CNN
F 4 "y" H 4200 5650 50  0001 C CNN "Mount"
F 5 "54601-908WPLF" H 4200 5650 50  0001 C CNN "Manufacturer ref."
	1    4200 5650
	-1   0    0    -1  
$EndComp
$Comp
L Connector:8P8C J?
U 1 1 5C742777
P 4200 7100
AR Path="/5C742777" Ref="J?"  Part="1" 
AR Path="/5C73A3FE/5C742777" Ref="J5"  Part="1" 
F 0 "J5" H 3871 7141 50  0000 R CNN
F 1 "8P8C_Shielded" H 3871 7050 50  0000 R CNN
F 2 "Connector_RJ:RJ45_Amphenol_54602-x08_Horizontal" V 4200 7125 50  0001 C CNN
F 3 "~" V 4200 7125 50  0001 C CNN
F 4 "y" H 4200 7100 50  0001 C CNN "Mount"
F 5 "54601-908WPLF" H 4200 7100 50  0001 C CNN "Manufacturer ref."
	1    4200 7100
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C74279E
P 2600 1800
AR Path="/5C74279E" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C74279E" Ref="#PWR0106"  Part="1" 
F 0 "#PWR0106" H 2600 1550 50  0001 C CNN
F 1 "GND" H 2605 1627 50  0000 C CNN
F 2 "" H 2600 1800 50  0001 C CNN
F 3 "" H 2600 1800 50  0001 C CNN
	1    2600 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C7427A4
P 2600 3250
AR Path="/5C7427A4" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C7427A4" Ref="#PWR0107"  Part="1" 
F 0 "#PWR0107" H 2600 3000 50  0001 C CNN
F 1 "GND" H 2605 3077 50  0000 C CNN
F 2 "" H 2600 3250 50  0001 C CNN
F 3 "" H 2600 3250 50  0001 C CNN
	1    2600 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C7427AA
P 2600 4700
AR Path="/5C7427AA" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C7427AA" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 2600 4450 50  0001 C CNN
F 1 "GND" H 2605 4527 50  0000 C CNN
F 2 "" H 2600 4700 50  0001 C CNN
F 3 "" H 2600 4700 50  0001 C CNN
	1    2600 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C7427B0
P 2600 7600
AR Path="/5C7427B0" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C7427B0" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 2600 7350 50  0001 C CNN
F 1 "GND" H 2605 7427 50  0000 C CNN
F 2 "" H 2600 7600 50  0001 C CNN
F 3 "" H 2600 7600 50  0001 C CNN
	1    2600 7600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C7427B6
P 2600 6150
AR Path="/5C7427B6" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C7427B6" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 2600 5900 50  0001 C CNN
F 1 "GND" H 2605 5977 50  0000 C CNN
F 2 "" H 2600 6150 50  0001 C CNN
F 3 "" H 2600 6150 50  0001 C CNN
	1    2600 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 3700 3150 3700
Wire Wire Line
	3150 3700 3150 3600
Wire Wire Line
	3000 2250 3150 2250
Wire Wire Line
	3150 2250 3150 2150
Wire Wire Line
	3000 800  3150 800 
Wire Wire Line
	3150 800  3150 700 
Wire Wire Line
	3000 5150 3150 5150
Wire Wire Line
	3150 5150 3150 5050
Wire Wire Line
	3000 6600 3150 6600
Wire Wire Line
	3150 6600 3150 6500
$Comp
L power:+12V #PWR?
U 1 1 5C7427C6
P 3150 700
AR Path="/5C7427C6" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C7427C6" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 3150 550 50  0001 C CNN
F 1 "+12V" H 3165 873 50  0000 C CNN
F 2 "" H 3150 700 50  0001 C CNN
F 3 "" H 3150 700 50  0001 C CNN
	1    3150 700 
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5C7427CC
P 3150 2150
AR Path="/5C7427CC" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C7427CC" Ref="#PWR0112"  Part="1" 
F 0 "#PWR0112" H 3150 2000 50  0001 C CNN
F 1 "+12V" H 3165 2323 50  0000 C CNN
F 2 "" H 3150 2150 50  0001 C CNN
F 3 "" H 3150 2150 50  0001 C CNN
	1    3150 2150
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5C7427D2
P 3150 3600
AR Path="/5C7427D2" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C7427D2" Ref="#PWR0113"  Part="1" 
F 0 "#PWR0113" H 3150 3450 50  0001 C CNN
F 1 "+12V" H 3165 3773 50  0000 C CNN
F 2 "" H 3150 3600 50  0001 C CNN
F 3 "" H 3150 3600 50  0001 C CNN
	1    3150 3600
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5C7427D8
P 3150 5050
AR Path="/5C7427D8" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C7427D8" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 3150 4900 50  0001 C CNN
F 1 "+12V" H 3165 5223 50  0000 C CNN
F 2 "" H 3150 5050 50  0001 C CNN
F 3 "" H 3150 5050 50  0001 C CNN
	1    3150 5050
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5C7427DE
P 3150 6500
AR Path="/5C7427DE" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C7427DE" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 3150 6350 50  0001 C CNN
F 1 "+12V" H 3165 6673 50  0000 C CNN
F 2 "" H 3150 6500 50  0001 C CNN
F 3 "" H 3150 6500 50  0001 C CNN
	1    3150 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1000 3800 1000
Wire Wire Line
	3000 900  3800 900 
Wire Wire Line
	3000 6700 3800 6700
Wire Wire Line
	3000 6800 3800 6800
Wire Wire Line
	3000 6900 3800 6900
Wire Wire Line
	3000 7000 3800 7000
Wire Wire Line
	3000 7100 3800 7100
Wire Wire Line
	3000 7200 3800 7200
Wire Wire Line
	3000 7300 3800 7300
Wire Wire Line
	3000 7400 3800 7400
Wire Wire Line
	3000 2350 3800 2350
Wire Wire Line
	3000 2450 3800 2450
Wire Wire Line
	3000 2550 3800 2550
Wire Wire Line
	3000 2650 3800 2650
Wire Wire Line
	3000 2750 3800 2750
Wire Wire Line
	3000 2850 3800 2850
Wire Wire Line
	3000 2950 3800 2950
Wire Wire Line
	3000 3050 3800 3050
Wire Wire Line
	3000 3800 3800 3800
Wire Wire Line
	3000 3900 3800 3900
Wire Wire Line
	3000 4000 3800 4000
Wire Wire Line
	3000 4100 3800 4100
Wire Wire Line
	3000 4200 3800 4200
Wire Wire Line
	3000 4300 3800 4300
Wire Wire Line
	3000 4400 3800 4400
Wire Wire Line
	3000 4500 3800 4500
Wire Wire Line
	3000 5250 3800 5250
Wire Wire Line
	3000 5350 3800 5350
Wire Wire Line
	3000 5450 3800 5450
Wire Wire Line
	3000 5550 3800 5550
Wire Wire Line
	3000 5650 3800 5650
Wire Wire Line
	3000 5750 3800 5750
Wire Wire Line
	3000 5850 3800 5850
Wire Wire Line
	3000 5950 3800 5950
Wire Wire Line
	3000 1100 3800 1100
Wire Wire Line
	3000 1600 3800 1600
Text Label 3300 900  0    50   ~ 0
red
Text Label 3300 1000 0    50   ~ 0
orange
Text Label 3300 1100 0    50   ~ 0
green
Text Label 3300 6700 0    50   ~ 0
~A~
Text Label 3300 6800 0    50   ~ 0
~B~
Text Label 3300 6900 0    50   ~ 0
~C~
Text Label 3300 7100 0    50   ~ 0
A
Text Label 3300 7200 0    50   ~ 0
B
Text Label 3300 7300 0    50   ~ 0
C
Text Label 3300 7400 0    50   ~ 0
D
Text Label 3350 2350 0    50   ~ 0
1A
Text Label 3350 2450 0    50   ~ 0
1B
Text Label 3350 2550 0    50   ~ 0
1C
Text Label 3350 2650 0    50   ~ 0
1D
Text Label 3350 2750 0    50   ~ 0
1E
Text Label 3350 2850 0    50   ~ 0
1F
Text Label 3350 2950 0    50   ~ 0
1G
Text Label 3350 3050 0    50   ~ 0
DOTS
Text Label 3350 3800 0    50   ~ 0
2A
Text Label 3350 3900 0    50   ~ 0
2B
Text Label 3350 4000 0    50   ~ 0
2C
Text Label 3350 4100 0    50   ~ 0
2D
Text Label 3350 4200 0    50   ~ 0
2E
Text Label 3350 4300 0    50   ~ 0
2F
Text Label 3350 4400 0    50   ~ 0
2G
Text Label 3350 5250 0    50   ~ 0
3A
Text Label 3350 5350 0    50   ~ 0
3B
Text Label 3350 5450 0    50   ~ 0
3C
Text Label 3350 5550 0    50   ~ 0
3D
Text Label 3350 5650 0    50   ~ 0
3E
Text Label 3350 5750 0    50   ~ 0
3F
Text Label 3350 5850 0    50   ~ 0
3G
Wire Wire Line
	6800 5400 6400 5400
Wire Wire Line
	2200 1500 1800 1500
Wire Wire Line
	2200 1400 1800 1400
Wire Wire Line
	2200 1300 1800 1300
Wire Wire Line
	2200 1200 1800 1200
Wire Wire Line
	2200 1600 1800 1600
Wire Wire Line
	2200 2950 1800 2950
Wire Wire Line
	2200 2850 1800 2850
Wire Wire Line
	2200 2750 1800 2750
Wire Wire Line
	2200 2650 1800 2650
Wire Wire Line
	2200 2550 1800 2550
Wire Wire Line
	2200 2450 1800 2450
Wire Wire Line
	2200 2350 1800 2350
Wire Wire Line
	2200 3050 1800 3050
Wire Wire Line
	2200 4300 1800 4300
Wire Wire Line
	2200 4200 1800 4200
Wire Wire Line
	2200 4100 1800 4100
Wire Wire Line
	2200 4000 1800 4000
Wire Wire Line
	2200 3900 1800 3900
Wire Wire Line
	2200 3800 1800 3800
Wire Wire Line
	2200 4500 1800 4500
Text Label 3300 7000 0    50   ~ 0
~D~
Wire Wire Line
	3000 1300 3800 1300
Wire Wire Line
	3000 1200 3800 1200
Wire Wire Line
	3000 1400 3800 1400
Text Label 3300 1200 0    50   ~ 0
red2
Text Label 3300 1300 0    50   ~ 0
orange2
Text Label 3300 1400 0    50   ~ 0
green2
Wire Wire Line
	3000 1500 3800 1500
Text Label 3300 1500 0    50   ~ 0
horn
Text Label 6400 5400 0    50   ~ 0
c_horn_p
$Comp
L Connector:Screw_Terminal_01x04 J?
U 1 1 5C756286
P 9800 4600
AR Path="/5C756286" Ref="J?"  Part="1" 
AR Path="/5C73A3FE/5C756286" Ref="J6"  Part="1" 
F 0 "J6" H 9880 4592 50  0000 L CNN
F 1 "Screw_Terminal_01x04" H 9880 4501 50  0000 L CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBVA_2,5_4-G_1x04_P5.00mm_Vertical" H 9800 4600 50  0001 C CNN
F 3 "~" H 9800 4600 50  0001 C CNN
F 4 "y" H 9800 4600 50  0001 C CNN "Mount"
	1    9800 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 4800 9400 4800
Wire Wire Line
	9400 4800 9400 4700
Wire Wire Line
	9600 4700 9400 4700
Connection ~ 9400 4700
Wire Wire Line
	9400 4700 9400 4600
Wire Wire Line
	9600 4600 9400 4600
Connection ~ 9400 4600
Wire Wire Line
	9400 4600 9400 4500
Wire Wire Line
	9600 4500 9400 4500
Connection ~ 9400 4500
$Comp
L power:+12V #PWR?
U 1 1 5C756298
P 9400 4350
AR Path="/5C756298" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C756298" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 9400 4200 50  0001 C CNN
F 1 "+12V" H 9415 4523 50  0000 C CNN
F 2 "" H 9400 4350 50  0001 C CNN
F 3 "" H 9400 4350 50  0001 C CNN
	1    9400 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 2800 7500 2700
Wire Wire Line
	7500 2700 8050 2700
Wire Wire Line
	7500 1600 7500 1450
Wire Wire Line
	7500 1450 8050 1450
Wire Wire Line
	7200 3000 7150 3000
Wire Wire Line
	7200 4150 7150 4150
$Comp
L power:GND #PWR?
U 1 1 5C760813
P 7500 2300
AR Path="/5C760813" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C760813" Ref="#PWR0119"  Part="1" 
F 0 "#PWR0119" H 7500 2050 50  0001 C CNN
F 1 "GND" H 7505 2127 50  0000 C CNN
F 2 "" H 7500 2300 50  0001 C CNN
F 3 "" H 7500 2300 50  0001 C CNN
	1    7500 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C760819
P 7500 3500
AR Path="/5C760819" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C760819" Ref="#PWR0120"  Part="1" 
F 0 "#PWR0120" H 7500 3250 50  0001 C CNN
F 1 "GND" H 7505 3327 50  0000 C CNN
F 2 "" H 7500 3500 50  0001 C CNN
F 3 "" H 7500 3500 50  0001 C CNN
	1    7500 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C76081F
P 7500 4650
AR Path="/5C76081F" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C76081F" Ref="#PWR0121"  Part="1" 
F 0 "#PWR0121" H 7500 4400 50  0001 C CNN
F 1 "GND" H 7505 4477 50  0000 C CNN
F 2 "" H 7500 4650 50  0001 C CNN
F 3 "" H 7500 4650 50  0001 C CNN
	1    7500 4650
	1    0    0    -1  
$EndComp
Text Label 8050 1450 2    50   ~ 0
red_p
Text Label 8050 2700 2    50   ~ 0
orange_p
Text Label 8050 3850 2    50   ~ 0
green_p
Text Label 6400 1800 0    50   ~ 0
c_red_p
Text Label 6400 4150 0    50   ~ 0
c_green_p
Text Label 6400 3000 0    50   ~ 0
c_orange_p
Entry Wire Line
	1700 1000 1800 900 
Entry Wire Line
	1700 1100 1800 1000
Entry Wire Line
	1700 1200 1800 1100
Entry Wire Line
	1700 1300 1800 1200
Entry Wire Line
	1700 1400 1800 1300
Entry Wire Line
	1700 1500 1800 1400
Entry Wire Line
	1700 1600 1800 1500
Entry Wire Line
	1700 1700 1800 1600
Text Notes 5100 1500 0    394  ~ 79
A
Text Notes 5100 2950 0    394  ~ 79
B
Text Notes 5100 4450 0    394  ~ 79
C
Text Notes 5100 5850 0    394  ~ 79
D
Text Notes 5100 7300 0    394  ~ 79
E
Text Label 1850 900  0    50   ~ 0
A0
Text Label 1850 1000 0    50   ~ 0
A1
Text Label 1850 1100 0    50   ~ 0
A2
Text Label 1850 1200 0    50   ~ 0
A3
Text Label 1850 1300 0    50   ~ 0
A4
Text Label 1850 1400 0    50   ~ 0
A5
Text Label 1850 1500 0    50   ~ 0
A6
Text Label 1850 1600 0    50   ~ 0
A7
Text Label 1800 2350 0    50   ~ 0
B0
Text Label 1800 2450 0    50   ~ 0
B1
Text Label 1800 2550 0    50   ~ 0
B2
Text Label 1800 2650 0    50   ~ 0
B3
Text Label 1800 2750 0    50   ~ 0
B4
Text Label 1800 2850 0    50   ~ 0
B5
Text Label 1800 2950 0    50   ~ 0
B6
Text Label 1800 3050 0    50   ~ 0
B7
Wire Wire Line
	1800 4400 2200 4400
Text Label 1800 3800 0    50   ~ 0
C0
Text Label 1800 3900 0    50   ~ 0
C1
Text Label 1800 4000 0    50   ~ 0
C2
Text Label 1800 4100 0    50   ~ 0
C3
Text Label 1800 4200 0    50   ~ 0
C4
Text Label 1800 4300 0    50   ~ 0
C5
Text Label 1800 4400 0    50   ~ 0
C6
Text Label 1800 4500 0    50   ~ 0
C7
Text Label 1800 5250 0    50   ~ 0
D0
Text Label 1800 5350 0    50   ~ 0
D1
Text Label 1800 5450 0    50   ~ 0
D2
Text Label 1800 5550 0    50   ~ 0
D3
Text Label 1800 5650 0    50   ~ 0
D4
Text Label 1800 5750 0    50   ~ 0
D5
Text Label 1800 5850 0    50   ~ 0
D6
Text Label 1800 5950 0    50   ~ 0
D7
Text Label 1800 7400 0    50   ~ 0
E0
Text Label 1800 7300 0    50   ~ 0
E1
Text Label 1800 7200 0    50   ~ 0
E2
Text Label 1800 7100 0    50   ~ 0
E3
Text Label 1800 7000 0    50   ~ 0
E4
Text Label 1800 6900 0    50   ~ 0
E5
Text Label 1800 6800 0    50   ~ 0
E6
Text Label 1800 6700 0    50   ~ 0
E7
Wire Wire Line
	2200 5850 1800 5850
Wire Wire Line
	2200 5750 1800 5750
Wire Wire Line
	2200 5650 1800 5650
Wire Wire Line
	2200 5550 1800 5550
Wire Wire Line
	2200 5450 1800 5450
Wire Wire Line
	2200 5350 1800 5350
Wire Wire Line
	2200 5250 1800 5250
Wire Wire Line
	2200 5950 1800 5950
Wire Bus Line
	1700 1800 1300 1800
Text Label 1300 1800 0    50   ~ 0
A[0..7]
Text HLabel 1300 1800 0    50   Input ~ 0
A[0..7]
Entry Wire Line
	1700 2450 1800 2350
Entry Wire Line
	1700 2550 1800 2450
Entry Wire Line
	1700 2650 1800 2550
Entry Wire Line
	1700 2750 1800 2650
Entry Wire Line
	1700 2850 1800 2750
Entry Wire Line
	1700 2950 1800 2850
Entry Wire Line
	1700 3050 1800 2950
Entry Wire Line
	1700 3150 1800 3050
Wire Bus Line
	1700 3250 1300 3250
Text HLabel 1300 3250 0    50   Input ~ 0
B[0..7]
Entry Wire Line
	1700 3900 1800 3800
Entry Wire Line
	1700 4000 1800 3900
Entry Wire Line
	1700 4100 1800 4000
Entry Wire Line
	1700 4200 1800 4100
Entry Wire Line
	1700 4300 1800 4200
Entry Wire Line
	1700 4400 1800 4300
Entry Wire Line
	1700 4500 1800 4400
Entry Wire Line
	1700 4600 1800 4500
Wire Bus Line
	1700 4700 1300 4700
Text HLabel 1300 4700 0    50   Input ~ 0
C[0..7]
Entry Wire Line
	1700 5350 1800 5250
Entry Wire Line
	1700 5450 1800 5350
Entry Wire Line
	1700 5550 1800 5450
Entry Wire Line
	1700 5650 1800 5550
Entry Wire Line
	1700 5750 1800 5650
Entry Wire Line
	1700 5850 1800 5750
Entry Wire Line
	1700 5950 1800 5850
Entry Wire Line
	1700 6050 1800 5950
Wire Bus Line
	1700 6150 1300 6150
Text HLabel 1300 6150 0    50   Input ~ 0
D[0..7]
Entry Wire Line
	1700 6800 1800 6700
Entry Wire Line
	1700 6900 1800 6800
Entry Wire Line
	1700 7000 1800 6900
Entry Wire Line
	1700 7100 1800 7000
Entry Wire Line
	1700 7200 1800 7100
Entry Wire Line
	1700 7300 1800 7200
Entry Wire Line
	1700 7400 1800 7300
Entry Wire Line
	1700 7500 1800 7400
Wire Bus Line
	1700 7600 1300 7600
Text Label 1300 7600 0    50   ~ 0
E
Text HLabel 1300 7600 0    50   Input ~ 0
E[0..7]
$Comp
L Device:R R3
U 1 1 5C1A1291
P 6950 1800
F 0 "R3" V 6743 1800 50  0000 C CNN
F 1 "100" V 6834 1800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 6880 1800 50  0001 C CNN
F 3 "~" H 6950 1800 50  0001 C CNN
F 4 "y" H 6950 1800 50  0001 C CNN "Mount"
F 5 "CFR-25JT-52-100R" H 6950 1800 50  0001 C CNN "Manufacturer ref."
	1    6950 1800
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5C1A1329
P 6950 3000
F 0 "R4" V 6743 3000 50  0000 C CNN
F 1 "100" V 6834 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 6880 3000 50  0001 C CNN
F 3 "~" H 6950 3000 50  0001 C CNN
F 4 "y" H 6950 3000 50  0001 C CNN "Mount"
F 5 "CFR-25JT-52-100R" H 6950 3000 50  0001 C CNN "Manufacturer ref."
	1    6950 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5C1A138D
P 6950 4150
F 0 "R5" V 6743 4150 50  0000 C CNN
F 1 "100" V 6834 4150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 6880 4150 50  0001 C CNN
F 3 "~" H 6950 4150 50  0001 C CNN
F 4 "y" H 6950 4150 50  0001 C CNN "Mount"
F 5 "CFR-25JT-52-100R" H 6950 4150 50  0001 C CNN "Manufacturer ref."
	1    6950 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 1800 6800 1800
Wire Wire Line
	6400 3000 6800 3000
Wire Wire Line
	6800 4150 6400 4150
$Comp
L Device:Q_NMOS_GDS Q1
U 1 1 5C2063FD
P 7400 1800
F 0 "Q1" H 7605 1846 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 7605 1755 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220F-3_Vertical" H 7600 1900 50  0001 C CNN
F 3 "https://www.mouser.it/datasheet/2/389/stp40nf03l-956435.pdf" H 7400 1800 50  0001 C CNN
F 4 "IRFZ24NPBF" H 7400 1800 50  0001 C CNN "Manufacturer ref."
F 5 "y" H 7400 1800 50  0001 C CNN "Mount"
	1    7400 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q2
U 1 1 5C20647D
P 7400 3000
F 0 "Q2" H 7605 3046 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 7605 2955 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220F-3_Vertical" H 7600 3100 50  0001 C CNN
F 3 "~" H 7400 3000 50  0001 C CNN
F 4 "y" H 7400 3000 50  0001 C CNN "Mount"
F 5 "IRFZ24NPBF" H 7400 3000 50  0001 C CNN "Manufacturer ref."
	1    7400 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q3
U 1 1 5C2064D3
P 7400 4150
F 0 "Q3" H 7605 4196 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 7605 4105 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220F-3_Vertical" H 7600 4250 50  0001 C CNN
F 3 "~" H 7400 4150 50  0001 C CNN
F 4 "y" H 7400 4150 50  0001 C CNN "Mount"
F 5 "IRFZ24NPBF" H 7400 4150 50  0001 C CNN "Manufacturer ref."
	1    7400 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5C23A467
P 7150 2050
AR Path="/5C23A467" Ref="R?"  Part="1" 
AR Path="/5C73A3FE/5C23A467" Ref="R6"  Part="1" 
F 0 "R6" H 7220 2096 50  0000 L CNN
F 1 "100k" H 7220 2005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7080 2050 50  0001 C CNN
F 3 "~" H 7150 2050 50  0001 C CNN
F 4 "y" H 7150 2050 50  0001 C CNN "Mount"
F 5 "CFR-25JR-52-100K" H 7150 2050 50  0001 C CNN "Manufacturer ref."
	1    7150 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 1800 7150 1800
Connection ~ 7150 1800
Wire Wire Line
	7150 1800 7200 1800
Wire Wire Line
	7150 1800 7150 1900
Wire Wire Line
	7150 2300 7500 2300
Wire Wire Line
	7500 2300 7500 2000
$Comp
L Device:R R?
U 1 1 5C2BB1D0
P 7150 3250
AR Path="/5C2BB1D0" Ref="R?"  Part="1" 
AR Path="/5C73A3FE/5C2BB1D0" Ref="R7"  Part="1" 
F 0 "R7" H 7220 3296 50  0000 L CNN
F 1 "100k" H 7220 3205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7080 3250 50  0001 C CNN
F 3 "~" H 7150 3250 50  0001 C CNN
F 4 "y" H 7150 3250 50  0001 C CNN "Mount"
F 5 "CFR-25JR-52-100K" H 7150 3250 50  0001 C CNN "Manufacturer ref."
	1    7150 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 3100 7150 3000
Connection ~ 7150 3000
Wire Wire Line
	7150 3000 7100 3000
Wire Wire Line
	7150 3500 7500 3500
Wire Wire Line
	7500 3500 7500 3200
Connection ~ 7500 3500
$Comp
L Device:R R?
U 1 1 5C2CB242
P 7150 4400
AR Path="/5C2CB242" Ref="R?"  Part="1" 
AR Path="/5C73A3FE/5C2CB242" Ref="R11"  Part="1" 
F 0 "R11" H 7220 4446 50  0000 L CNN
F 1 "100k" H 7220 4355 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7080 4400 50  0001 C CNN
F 3 "~" H 7150 4400 50  0001 C CNN
F 4 "y" H 7150 4400 50  0001 C CNN "Mount"
F 5 "CFR-25JR-52-100K" H 7150 4400 50  0001 C CNN "Manufacturer ref."
	1    7150 4400
	1    0    0    -1  
$EndComp
Connection ~ 7150 4150
Wire Wire Line
	7150 4150 7100 4150
Wire Wire Line
	7150 4650 7500 4650
Wire Wire Line
	7500 4650 7500 4350
Connection ~ 7500 4650
Text Label 3300 1600 0    50   ~ 0
jolly0
Text Label 3350 4500 0    50   ~ 0
jolly1
Text Label 3350 5950 0    50   ~ 0
jolly2
$Comp
L Connector:Screw_Terminal_01x04 J7
U 1 1 5C2F1A6F
P 9800 5150
F 0 "J7" H 9880 5142 50  0000 L CNN
F 1 "Screw_Terminal_01x04" H 9880 5051 50  0000 L CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBVA_2,5_4-G_1x04_P5.00mm_Vertical" H 9800 5150 50  0001 C CNN
F 3 "~" H 9800 5150 50  0001 C CNN
F 4 "y" H 9800 5150 50  0001 C CNN "Mount"
	1    9800 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 5150 9350 5150
Wire Wire Line
	9600 5250 9350 5250
Wire Wire Line
	9600 5350 9350 5350
Wire Wire Line
	9600 5050 9350 5050
Text Label 7850 5100 2    50   ~ 0
horn_p
Text Label 9350 5150 0    50   ~ 0
red_p
Text Label 9350 5250 0    50   ~ 0
orange_p
Text Label 9350 5350 0    50   ~ 0
green_p
Text Label 9350 5050 0    50   ~ 0
horn_p
Wire Wire Line
	2200 6700 1800 6700
Wire Wire Line
	2200 7300 1800 7300
Wire Wire Line
	2200 7200 1800 7200
Wire Wire Line
	2200 7100 1800 7100
Wire Wire Line
	2200 7000 1800 7000
Wire Wire Line
	2200 6900 1800 6900
Wire Wire Line
	2200 6800 1800 6800
Wire Wire Line
	2200 7400 1800 7400
Text HLabel 6400 1800 0    50   Input ~ 0
REDP
Text HLabel 6400 3000 0    50   Input ~ 0
ORANGEP
Text HLabel 6400 4150 0    50   Input ~ 0
GREENP
Text HLabel 6400 5400 0    50   Input ~ 0
HORNP
Wire Wire Line
	1800 1100 2200 1100
Wire Wire Line
	1800 900  2200 900 
Wire Wire Line
	1800 1000 2200 1000
$Comp
L power:GND #PWR?
U 1 1 5C3B0E8E
P 7500 5900
AR Path="/5C3B0E8E" Ref="#PWR?"  Part="1" 
AR Path="/5C73A3FE/5C3B0E8E" Ref="#PWR0116"  Part="1" 
F 0 "#PWR0116" H 7500 5650 50  0001 C CNN
F 1 "GND" H 7505 5727 50  0000 C CNN
F 2 "" H 7500 5900 50  0001 C CNN
F 3 "" H 7500 5900 50  0001 C CNN
	1    7500 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5C3B0E94
P 6950 5400
F 0 "R1" V 6743 5400 50  0000 C CNN
F 1 "100" V 6834 5400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 6880 5400 50  0001 C CNN
F 3 "~" H 6950 5400 50  0001 C CNN
F 4 "y" H 6950 5400 50  0001 C CNN "Mount"
F 5 "CFR-25JT-52-100R" H 6950 5400 50  0001 C CNN "Manufacturer ref."
	1    6950 5400
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NMOS_GDS Q4
U 1 1 5C3B0E9B
P 7400 5400
F 0 "Q4" H 7605 5446 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 7605 5355 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220F-3_Vertical" H 7600 5500 50  0001 C CNN
F 3 "~" H 7400 5400 50  0001 C CNN
F 4 "y" H 7400 5400 50  0001 C CNN "Mount"
F 5 "IRFZ24NPBF" H 7400 5400 50  0001 C CNN "Manufacturer ref."
	1    7400 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5C3B0EA2
P 7150 5650
AR Path="/5C3B0EA2" Ref="R?"  Part="1" 
AR Path="/5C73A3FE/5C3B0EA2" Ref="R2"  Part="1" 
F 0 "R2" H 7220 5696 50  0000 L CNN
F 1 "100k" H 7220 5605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7080 5650 50  0001 C CNN
F 3 "~" H 7150 5650 50  0001 C CNN
F 4 "y" H 7150 5650 50  0001 C CNN "Mount"
F 5 "CFR-25JR-52-100K" H 7150 5650 50  0001 C CNN "Manufacturer ref."
	1    7150 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 5900 7500 5900
Connection ~ 7500 5900
Wire Wire Line
	9400 4350 9400 4500
Text Notes 11000 2100 2    50   ~ 0
100k resistors are pull-down for\nMOSFETs. Not mandatory.\n100 Ohm resistors are for gate\nprotection, but may be changed\nwith shorts.
Text Notes 11000 1650 2    50   ~ 0
Le resistenza da 100k sono pull-down\nper i MOSFETs. Non obbligatorie.\nLe resistenze da 100 Ohm sono di protezione\nper i gate, ma possono essere eventualemten\nsostituite con ponticelli.
Connection ~ 7500 2300
Wire Wire Line
	7150 2200 7150 2300
Wire Wire Line
	7150 3500 7150 3400
Wire Wire Line
	7150 4250 7150 4150
Wire Wire Line
	7150 4550 7150 4650
Wire Wire Line
	7100 5400 7150 5400
Wire Wire Line
	7150 5900 7150 5800
Wire Wire Line
	7150 5500 7150 5400
Connection ~ 7150 5400
Wire Wire Line
	7150 5400 7200 5400
Wire Wire Line
	7500 5600 7500 5900
Wire Wire Line
	7500 5200 7500 5100
Wire Wire Line
	7500 5100 7850 5100
Wire Wire Line
	7500 3950 7500 3850
Wire Wire Line
	7500 3850 8050 3850
Wire Bus Line
	1700 1000 1700 1800
Wire Bus Line
	1700 2450 1700 3250
Wire Bus Line
	1700 3900 1700 4700
Wire Bus Line
	1700 5350 1700 6150
Wire Bus Line
	1700 6800 1700 7600
$EndSCHEMATC
